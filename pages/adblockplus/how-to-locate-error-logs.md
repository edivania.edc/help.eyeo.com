title=How to locate error logs
description=Error logs help us identify exactly where the problem is when you're experiencing issues with Adblock Plus. Here's how you can access them.
template=article
product_id=abp
category=Troubleshooting & Reporting

At times in troubleshooting we will need to take an extra step to fully isolate the issue. Error logs tell us that extra piece of information about where the problem lies. Follow the steps below to access those error logs. 

<section class="platform-chrome" markdown="1">
## Chrome

1. Right click the ABP icon
1. Click on **Manage Extensions**
1. Toggle **Developer Mode** to on
1. Click background page located under inspect views section
1. Click on the **Console** tab
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. Go to **about:addons**
1. Click on **Extensions**
1. Click on the settings gear to right of manage extensions
1. Click **debug add-ons**
1. Click **Inspect** next to Adblock Plus
1. Click on the **Console** Tab
</section>

<section class="platform-safari" markdown="1">
## Safari

You must first select the option to show the Develop Menu in Safari.

1. Click on Safari in the left corner of the page next to the apple icon
1. Click on **Preferences**
1. Click on **Advanced**
1. Check option to **Show Develop Menu in Menu Bar**

To open the console:

1. Click on **Develop** in the menu bar
1. Select **Show Web Inspector**
1. Click on **Console** Tab
1. Errors will appear in red
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. Right click on a webpage
1. Choose **Inspect element**
1. Click on **Console** tab
1. Errors will appear in red
</section>
